# coding=utf-8
file = open('script.rpy', 'r')
text = file.read()

start_find = text.find('label')
end_find = text.rfind('"')
keywords = []

while start_find <= end_find:
    find_perenos = text.find('\n', start_find)
    find_next_perenos = text.find('\n', find_perenos + 1)
    find_kaw = text.find('"', find_perenos, find_next_perenos)
    if find_kaw > 0:
        keywords.append(text[find_perenos + 1:find_kaw - 1])
        start_find = find_next_perenos + 1
    else:
        start_find = find_next_perenos + 1

vsego = len(keywords)
i = 0
keywords2 = []
while i < vsego:
    keywords2.append(str.strip(keywords[i]))
    i += 1
keywords = keywords2

keywords = sorted(set(keywords))

define = []
find_deifne = text.find('define')
find_ravno = text.find(' =')
end_deifne = text.rfind('define')

while find_deifne <= end_deifne:
    find_deifne = text.find('define', find_deifne)
    find_ravno = text.find(' =', find_deifne)
    define.append(text[find_deifne + 7:find_ravno])
    find_deifne += 1

define = sorted(set(define))

define_result = list(set(keywords) - set(define))
define_result.remove('')

# Разделяем текст на список используя разделитель: "
text = text.split('"')

# Находим первый элемент в списке который содержит 'label start'
a = -1
l_s = -1
while a < 0:
    l_s += 1
    a = text[l_s]
    a = a.find('label start')

print("Находим место начала выгрузки.\nСлова 'label start' содержит " + str(l_s) + " элемент в списке.\n")

# Выгружаем в отдельный список все чётные строки, после строки с 'label start'

start_en = l_s
text_en = []

while start_en < len(text)-1:
    # Проверям есть ли в строке соотвествие из стоп-списка. Если есть то выставляем флаг
    for x in define_result:
        flag = 0
        x2 = text[start_en].find(x)
        if x2 > 0:
            flag = 1
            break
        if text[start_en+1].find('mp3') > 0:
            flag = 1
            break
    # Если в предыдущей строке флага нет записываем строчку в отделный список "text_en"
    if flag != 1:
        text_en.append(text[start_en+1])
        start_en += 2
    else:
        start_en += 2


# Создаём новый файл и записываем в него все строки из скрипта требующие перевода.
file_en = open('text_en.txt','w')
for x3 in text_en:
    file_en.write(x3 + '\n')
file_en.close()
print("Созаём файл 'text_en.txt' в который записываем все строки скрипта требующие перевода.\n"
      "Всего выгружено "+ str(len(text_en))+" строк текста.")


# Открываем файл с готовым переводом.
file_ru = open('text_ru.txt','r')
text_ru = file_ru.read()
text_ru = text_ru.split('\n')

start_en = l_s
index_ru = 0

while start_en < len(text)-1:
    # Проверям есть ли в строке соотвествие из стоп-списка. Если есть то выставляем флаг
    for x in define_result:
        flag = 0
        x2 = text[start_en].find(x)
        if x2 > 0:
            flag = 1
            break
        if text[start_en+1].find('mp3') > 0:
            flag = 1
            break
    # Если в предыдущей строке флага нет заменяем строку на строку из файла с готовым переводом.
    if flag != 1:
        text[start_en+1] = text_ru[index_ru]
        index_ru += 1
        start_en += 2
    else:
        start_en += 2

# Преобразуем список элементов обратно в текст используя разделитель: "

text = '"'.join(text)

# Создаём файл и записываем в него полученный текст.
script_ru = open('script_ru.txt','w')
script_ru.write(text)
script_ru.close()
